﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Electronics.aspx.cs" Inherits="Electronics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftHolder" Runat="Server">
    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" Text="TV&amp; Video"></asp:LinkButton>
        <br />
    <asp:LinkButton ID="LinkButton2" runat="server" Text="Laptops & Computers" OnClick="LinkButton2_Click"></asp:LinkButton>
    <br />
    <asp:LinkButton ID="LinkButton3" runat="server" Text="Digital Cameras" OnClick="LinkButton3_Click"></asp:LinkButton>
    </asp:Content>

     <asp:Content ID="Content2" ContentPlaceHolderID="RightHolder" Runat="Server">
         <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="3">
        <asp:View ID="View1" runat="server">
            <asp:Label ID="Label2" runat="server" Text="TV & Video" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
            <br /><br />

            <p> &nbsp &nbsp Please select from our TV sets offer: </p>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/TV.png" Height="323px" Width="610px" style="margin-left: 81px" />
         </asp:View>
 
        <asp:View ID="View2" runat="server">
         <asp:Label ID="Label1" runat="server" Text="Laptops & Computers" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
         <br /><br />
          <p> &nbsp &nbsp Please select from our Laptops offer: </p>
          <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Laptops.png" Height="323px" Width="610px" style="margin-left: 81px" />
       </asp:View>
       <asp:View ID="View3" runat="server">
           <asp:Label ID="Label3" runat="server" Text="Camera & CamCoders" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
                    <br /><br />
          <p> &nbsp &nbsp Please select from our Cameras offer: </p>
           <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Cameras.png" Height="332px" Width="610
               px" style="margin-left: 81px" />
         </asp:View>
           <asp:View ID="View4" runat="server">
           <asp:Label ID="Label4" runat="server" Text="Electronics Department" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
           <br /><br />
          <p> &nbsp &nbsp Please select an option from the left menu </p><asp:Image ID="Image4" runat="server" ImageUrl="~/Images/menuElectronics.png" Height="264px" Width="621px" style="margin-left: 81px"/>
           
         </asp:View>

    </asp:MultiView>

    </asp:Content>

