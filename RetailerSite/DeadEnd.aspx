﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DeadEnd.aspx.cs" Inherits="DeadEnd" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <br /> <br />
            <asp:Label ID="Label1" runat="server" Text="Thank you for visiting this page!" Font-Underline="True" Font-Size="XX-Large"
                            Font-Bold="True"></asp:Label>
            <br /><br />
            <asp:HyperLink ID="HyperLink1" runat="server" Text= "<< Retun to Site" NavigateUrl="~/Home.aspx" Font-Bold="True" Font-Size="Large"></asp:HyperLink>

        </div>
    </form>
</body>
</html>
