﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Appliances.aspx.cs" Inherits="appliances" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftHolder" Runat="Server">
    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" Text="Large Appliances"></asp:LinkButton>
        <br />
    <asp:LinkButton ID="LinkButton2" runat="server" Text="Dishwashers" OnClick="LinkButton2_Click"></asp:LinkButton>
    <br />
    <asp:LinkButton ID="LinkButton3" runat="server" Text="Vaccums Cleaners" OnClick="LinkButton3_Click"></asp:LinkButton>
    </asp:Content>

     <asp:Content ID="Content2" ContentPlaceHolderID="RightHolder" Runat="Server">
         <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="3">
        <asp:View ID="View1" runat="server">
            <asp:Label ID="Label2" runat="server" Text="Large Appliances" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
            <br /><br />

            <p> &nbsp &nbsp Please select from our Large Appliances offer: </p>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/LargeAppliances.png" Height="323px" Width="610px" style="margin-left: 81px" />
         </asp:View>
 
        <asp:View ID="View2" runat="server">
         <asp:Label ID="Label1" runat="server" Text="Dishwashers" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
         <br /><br />
          <p> &nbsp &nbsp Please select from our Dishwashers offer: </p>
          <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Dishwashers.png" Height="323px" Width="610px" style="margin-left: 81px" />
       </asp:View>
       <asp:View ID="View3" runat="server">
           <asp:Label ID="Label3" runat="server" Text="Vaccum Cleaners" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
           <br /><br />
          <p> &nbsp &nbsp Please select from our VaccumCleaners offer: </p>
           <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/VaccumCleaners.png" Height="365px" Width="624px" style="margin-left: 81px" />
         </asp:View>
       <asp:View ID="View4" runat="server">
           <asp:Label ID="Label4" runat="server" Text="Appliancies Department" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
           <br /><br />
          <p> &nbsp &nbsp Please select an option from the left menu </p>
           <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/menuAppliances.png" Height="264px" Width="621px" style="margin-left: 81px" />
         </asp:View>
    </asp:MultiView>

    </asp:Content>



