﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftHolder" Runat="Server">
    <asp:LinkButton ID="LinkButton1" runat="server" Text="About us" OnClick="LinkButton1_Click" ></asp:LinkButton>
    <br />
    <asp:LinkButton ID="LinkButton2" runat="server" Text="Careers" OnClick="LinkButton2_Click"></asp:LinkButton>
    <br />
    <asp:LinkButton ID="LinkButton3" runat="server" Text="Contact us" OnClick="LinkButton3_Click"></asp:LinkButton>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RightHolder" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="3">
        <asp:View ID="View1" runat="server">
            <asp:Label ID="Label2" runat="server" Text="About us" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
            <br /><br />

            <p> &nbsp &nbsp Welcome to S-mart, your number one source for all things products: Clothing , Electronics, Grocery, 
                Auto Parts and Sport and Outdor supplies. </p>
            <p> &nbsp &nbsp We're dedicated to giving you the  best  producta on the market, with
                           a focus on three characteristics: dependability, customer service and uniqueness. </p>
 
        </asp:View>
       <asp:View ID="View2" runat="server">
         <asp:Label ID="Label1" runat="server" Text="Career Opportunities" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
         <br /><br />
         <p> &nbsp &nbsp Career paths with S-MART include information technology, distribution, buying, accounting,
             finance, marketing, human resources and more. </p><br />
           <asp:Button ID="Button1" runat="server" Text="APPLY NOW" Font-Bold="True" Font-Size="Large" OnClick="Button1_Click" Width="147px" />
       </asp:View>
       <asp:View ID="View3" runat="server">
           <asp:Label ID="Label3" runat="server" Text="Contact us" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
           <p> &nbsp &nbsp We welcome the chance to hear from you! </p>
           <p> &nbsp &nbsp Have a thought to share or some feedback on your experience?
               Please complete the form below and let us know. We value your feedback with an aim to continually improve.</p>
           <p> &nbsp &nbsp We want to help you find your way. </p>
           <p> &nbsp &nbsp Please consider if any of the topics below help guide you in the right direction. If you do not find what you need,
                            simply complete the form below and our team will be sure to follow up with you shortly. </p>
           <asp:Button ID="Button2" runat="server" Text="Support Department" Font-Bold="True" Width="176px" OnClick="Button2_Click" /><br />
           <asp:Button ID="Button3" runat="server" Text="Sales Department" Font-Bold="True" Width="176px" OnClick="Button3_Click" /><br />
         </asp:View>
        <asp:View ID="View4" runat="server">
        <br /><br />
        <asp:Label ID="Label5" runat="server" Text="WELCOME TO S-MART YOUR FAVORITE STORE !" Font-Underline="false" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
       <p> &nbsp &nbsp Plese select an option from the menu on the left. </p>
       </asp:View>
    </asp:MultiView>
</asp:Content>


