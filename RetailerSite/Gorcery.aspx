﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Gorcery.aspx.cs" Inherits="Gorcery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LeftHolder" Runat="Server">
    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" Text="Vegetables"></asp:LinkButton>
        <br />
    <asp:LinkButton ID="LinkButton2" runat="server" Text="Fruits" OnClick="LinkButton2_Click"></asp:LinkButton>
    <br />
    <asp:LinkButton ID="LinkButton3" runat="server" Text="Snacks & Chips" OnClick="LinkButton3_Click"></asp:LinkButton>
    </asp:Content>

     <asp:Content ID="Content2" ContentPlaceHolderID="RightHolder" Runat="Server">
         <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="3">
        <asp:View ID="View1" runat="server">
            <asp:Label ID="Label2" runat="server" Text="Vegetables" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
            <br /><br />

            <p> &nbsp &nbsp Please select from our Vegetables offer: </p>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Vegetables.png" Height="323px" Width="610px" style="margin-left: 81px" />
         </asp:View>
 
        <asp:View ID="View2" runat="server">
         <asp:Label ID="Label1" runat="server" Text="Fruits" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
         <br /><br />
          <p> &nbsp &nbsp Please select from our Fruits offer: </p>
          <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Fruits.png" Height="323px" Width="610px" style="margin-left: 81px" />
       </asp:View>
       <asp:View ID="View3" runat="server">
           <asp:Label ID="Label3" runat="server" Text="Snacks & Chips" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
           <br /><br />
          <p> &nbsp &nbsp Please select from our Snacks & Chips offer: </p>
           <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Snacks.png" Height="365px" Width="624px" style="margin-left: 81px" />
         </asp:View>
       <asp:View ID="View4" runat="server">
           <asp:Label ID="Label4" runat="server" Text="Gocery Department" Font-Underline="True" Font-Size="XX-Large" Font-Bold="True"></asp:Label>
           <br /><br />
          <p> &nbsp &nbsp Please select an option from the left menu. </p>
           <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/menuGrocery.png" Height="193px" Width="621px" style="margin-left: 81px" />
         </asp:View>
    </asp:MultiView>

    </asp:Content>


